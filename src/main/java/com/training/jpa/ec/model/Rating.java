package com.training.jpa.ec.model;

public enum Rating {
    Difficult, Easy, Medium, Varies;
}
