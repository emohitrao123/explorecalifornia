package com.training.jpa.ec.service;

import com.training.jpa.ec.model.Rating;
import com.training.jpa.ec.model.Tour;
import com.training.jpa.ec.model.TourPackage;
import com.training.jpa.ec.repo.TourPackageRepository;
import com.training.jpa.ec.repo.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourService {

    @Autowired
    private TourRepository tourRepository;

    @Autowired
    private TourPackageRepository tourPackageRepository;

    //create tour
    public Tour createTour (String title, String description, String blurb, Integer price,
                            String duration, String bullets, String keywords, Rating rating,
                            String tourPacakageName){
         //step 1
           TourPackage tourPackage = tourPackageRepository.findByName(tourPacakageName).
                   orElseThrow(()-> new RuntimeException("Tour Pacakage Not Found" +tourPacakageName));
           //step 2
        return tourRepository.save(new Tour(title,description,blurb,price,duration,bullets,keywords,rating,tourPackage));

    }
}
