package com.training.jpa.ec.service;

import com.training.jpa.ec.model.Tour;
import com.training.jpa.ec.model.TourPackage;
import com.training.jpa.ec.repo.TourPackageRepository;
import com.training.jpa.ec.repo.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourPackageService {

    @Autowired
    private TourPackageRepository tourPackageRepository;



    //create tourpackage
    public TourPackage createTourPackage(String code, String name){
         tourPackageRepository.findById(code).
                orElseThrow(()->new RuntimeException("TourPackage Not Found" +name));
        return tourPackageRepository.save(new TourPackage(code,name));
    }

    //to find the count
    public long numberOfTourPacakages(){
        return tourPackageRepository.count();
    }

    //to retrieve the name of all tour pacakages
    public Iterable<TourPackage> nameOfTourPackages(){
        return tourPackageRepository.findAll();
    }
}
